import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.home import HomePage
from pages.core.bib import BibPage
from pages.core.recent_arrivals import RecentArrivalsPage

@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C46959: 'On Order' Tab")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C46959", "TestRail")
class TestC46959:
    def test_C46959(self):
        home_page = HomePage(self.driver, configuration.system.base_url + "/explore/recent_arrivals").open()
        recent_arrivals_page = RecentArrivalsPage(self.driver)
        recent_arrivals_page.on_order.click()
        recent_arrivals_page.wait.until(lambda results_found: recent_arrivals_page.is_results_displayed)
        recent_arrivals_page.results[0].jacket_link.click()
        bib_page = BibPage(self.driver)
        bib_page.wait.until(lambda s: bib_page.circulation.is_place_hold_button_displayed)
        bib_page.circulation.item_availability_status.text.should.equal("On order")
