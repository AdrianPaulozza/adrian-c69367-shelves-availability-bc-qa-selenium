import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.home import HomePage
from pages.core.v2.search_results import SearchResultsPage
from pages.core.holds import HoldsPage

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C46018: Select an eBook on Holds page, pause the bib")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C46018", "TestRail")
class TestC46018:
    def test_C46018(self):
        home_page = HomePage(self.driver, "https://coaldale.demo.bibliocommons.com").open()
        home_page.header.log_in("21817002369272", "1992")

        if home_page.header.is_collapsible_search_trigger_displayed:
            home_page.header.collapsible_search_trigger.click()
        search_term = "eBook"
        search_results_page = home_page.header.search_for(search_term) # , advanced_search = True)

        # Wait for search results to be greater than 0:
        search_results_page.wait.until(lambda s: (len(search_results_page.search_result_items) > 0))

        # Scan through search results, look for an item that shows "request this download" button
        valid_result = -1

        for i in search_results_page.search_result_items:

            if i.is_request_this_download_displayed:
                valid_result = i
                break

        # Attempt to click "request this download"
        # It is assumed that if this fails, it is because no bib was found on the page that has this button present.
        try:
            valid_result.request_this_download.click()
        except Exception:
            raise Exception('No holdable digital items found in search results. Check account and search terms on library branch')

        search_results_page.wait.until(lambda s: valid_result.is_place_digital_hold_displayed)
        valid_result.place_digital_hold.click()

        # Go to holds page and pause the bib
        search_results_page.header.login_state_user_logged_in.click()
        search_results_page.header.on_hold.click()
        holds_page = HoldsPage(self.driver)

        try:
            holds_page.wait.until(lambda s: (len(holds_page.items) > 0))
        except Exception:
            self.driver.refresh()
            holds_page.wait.until(lambda s: (len(holds_page.items) > 0))

        holds_page.items[0].pause.click()

        # Click to open the date picker, click "today" inside of the date picker
        holds_page.wait.until(lambda s: holds_page.items[0].is_suspend_end_date_displayed)
        holds_page.items[0].suspend_end_date.click()

        holds_page.wait.until(lambda s: holds_page.date_picker.today is not False)
        holds_page.date_picker.today.click()

        # Wait until the pause holds button appears - click and verify one paused hold is present
        holds_page.wait.until(lambda s: holds_page.items[0].is_pause_holds_displayed)
        holds_page.items[0].pause_holds.click()
        holds_page.wait.until(lambda s: holds_page.status.text == "1 Paused")
        holds_page.status.text.should.equal("1 Paused")

        # Test passed - Cancel the hold placed during the test and finish:
        holds_page = HoldsPage(self.driver)
        holds_page.items[0].cancel.click()
        holds_page.wait.until(lambda s: holds_page.items[0].is_cancel_holds_displayed)
        holds_page.items[0].cancel_holds.click()
        holds_page.wait.until(lambda s: holds_page.is_empty_page_displayed)
