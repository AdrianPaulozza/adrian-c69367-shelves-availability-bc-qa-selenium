import pytest
import allure
import urllib.parse
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.home import HomePage
from pages.core.recent_arrivals import RecentArrivalsPage

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C47051: Canned search links on New Titles page")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C47051", "TestRail")
class TestC47051:
    def test_C47051(self):
        self.base_url = "https://sfpl.demo.bibliocommons.com"

        home_page = HomePage(self.driver, self.base_url).open()
        home_page.header.explore.click()
        home_page.header.new_titles.click()
        recent_arrivals_page = RecentArrivalsPage(self.driver)
        # Find a random list that has an ampersand in the list name:
        for list in recent_arrivals_page.panels:
            for link in list.links:
                if "&" in link.text:
                    print(link.text)
                    list_with_special_characters = link
                    break
        # We need to URL encode the list name to use it effectively in our assertion:
        encoded_list_name = urllib.parse.quote_plus(list_with_special_characters.text, safe = '', encoding = None, errors = None)
        list_with_special_characters.click()
        # The list URL should contain the URL encoded name of the list we clicked on:
        self.driver.current_url.should.contain(encoded_list_name)
