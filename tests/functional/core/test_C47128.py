import pytest
import allure
import re
import sure
import sys
sys.path.append('tests')
import configuration.system
import configuration.user

from pages.core.bib import BibPage

@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C47128: Add a tag")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C47128", "TestRail")
class TestC47128:
    def test_C47128(self):
        self.item_id = "719979001"
        bib_page = BibPage(self.driver, configuration.system.base_url, item_id = self.item_id).open()
        bib_page.header.log_in(configuration.user.name, configuration.user.password)
        bib_page.ugc_metadata.add_tags.click()
        tag_body = "automagic"
        bib_page.overlay.add_tags.genre_field.send_keys(tag_body)
        bib_page.overlay.add_tags.post_tags.click()
        # Remove the (#) counter from the tag link's text to assert equality:
        tag_link_text = re.sub('\s\(\d+\)', '', bib_page.ugc_metadata.tag_links[0].text)
        tag_body.should.equal(tag_link_text)

        # Delete the tag added during the test run:
        bib_page = BibPage(self.driver, configuration.system.base_url, item_id = self.item_id).open()
        bib_page.ugc_metadata.add_tags.click()
        bib_page.overlay.add_tags.remove_tags[0].click()
        bib_page.overlay.add_tags.post_tags.click()
