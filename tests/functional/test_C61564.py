import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.v2.search_results import SearchResultsPage
from utils.url_helper import SearchPageHelper
from pages.core.v2.holds import HoldsPage

@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C61564: Select a hold, not single click holds and cancel(not logged in)")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C61564", "TestRail")
class TestC61564:
    def test_C61564(self):
        # this test used to be c45998, but that case was removed from test rail and replaced as C61564
        # print(self.search_item_url)
        # print(self.item)

        self.search_item_url, self.item = \
            SearchPageHelper.search_page_specific(self, term="Harry Potter", search_type="keyword", format="BK",

                            available=1, holds=0, base_url="https://coaldale.demo.bibliocommons.com")

        search_page = SearchResultsPage(self.driver, self.search_item_url).open()

        search_page.search_result_items[0].place_hold.click()
        search_page.overlay.search_page_login.log_in("coaldaletest", "1992")
        search_page.wait.until(lambda a: search_page.search_result_items[0].is_confirm_hold_displayed)
        search_page.search_result_items[0].confirm_hold.click()
        search_page.wait.until(lambda a: search_page.search_result_items[0].is_cancel_hold_displayed)

        search_page.header.login_state_user_logged_in.click()
        search_page.header.on_hold.click()
        holds_page = HoldsPage(self.driver)

        holds_page.items[0].bib_title.text.should.contain(self.item.title)

        holds_page = HoldsPage(self.driver)
        holds_page.items[0].cancel.click()
        holds_page.items[0].confirm.click()
