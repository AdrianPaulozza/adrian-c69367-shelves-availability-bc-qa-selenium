from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pages.core.base import BasePage
# from pypom import Page, Region

# https://<library>.<environment>.bibliocommons.com/fines
class FinesPage(BasePage):

    URL_TEMPLATE = "/fines"

    _heading_locator = (By.ID, "content-start") # "Fees" heading
    _fines_payable_locator = (By.CSS_SELECTOR, "[testid='text_finespayable']")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def fines_payable(self):
        return self.find_element(*self._fines_payable_locator)
