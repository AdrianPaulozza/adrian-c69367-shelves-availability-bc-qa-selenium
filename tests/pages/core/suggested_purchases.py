from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from .base import BasePage
from pypom import Region

# https://<library>.<environment>.bibliocommons.com/suggested_purchases
class SuggestedPurchasesPage(BasePage):

    URL_TEMPLATE = "/suggested_purchases"

    _root_locator = (By.CSS_SELECTOR, "[data-test-id='suggested_purchases_page']")
    _cancel_suggestion_locator = (By.CSS_SELECTOR, "[data-test-id='cancel-suggestion']")
    _cancel_confirmation_locator = (By.CSS_SELECTOR, "[data-test-id='confirmation-proceed-button']")
    _suggestion_counter_locator = (By.CSS_SELECTOR, "[data-test-id='suggestion-count']")
    _remove_suggestion_locator = (By.CSS_SELECTOR, "[data-test-id='remove-suggestion']")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._root_locator).is_present()
        except NoSuchElementException:
            return False

    @property
    def cancel_suggestion(self):
        return self.find_element(*self._cancel_suggestion_locator)

    @property
    def is_cancel_suggestion_displayed(self):
        try:
            return self.find_element(*self._cancel_suggestion_locator).is_displayed()
        except NoSuchElementException:
            return False


    @property
    def cancel_confirmation(self):
        return self.find_element(*self._cancel_confirmation_locator)

    @property
    def is_cancel_confirmation_displayed(self):
        try:
            return self.find_element(*self._cancel_confirmation_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def suggestion_counter(self):
        return self.find_element(*self._suggestion_counter_locator)

    @property
    def remove_suggestion(self):
        return self.find_element(*self._remove_suggestion_locator)

    @property
    def is_remove_suggestion_displayed(self):
        try:
            return self.find_element(*self._remove_suggestion_locator).is_displayed()
        except NoSuchElementException:
            return False
