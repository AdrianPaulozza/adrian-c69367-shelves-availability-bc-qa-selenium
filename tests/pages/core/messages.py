from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from .base import BasePage
from pypom import Region

# https://<library>.<environment>.bibliocommons.com/messages
class MessagesPage(BasePage):

    URL_TEMPLATE = "/messages"

    _my_inbox_locator = (By.CSS_SELECTOR, "a[href='/messages']")
    _message_locator = (By.CSS_SELECTOR, "tr[testid*='message_']")
    _delete_locator = (By.CSS_SELECTOR, "[testid='button_deletemessage']")
    _top_message_container_locator = (By.CSS_SELECTOR, "[data-test-id='top-message-container']")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._my_inbox_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def messages(self):
        return [self.Message(self, element) for element in self.find_elements(*self._message_locator)]

    @property
    def delete(self):
        return self.find_element(*self._delete_locator)

    @property
    def top_message_container(self):
        return self.find_element(*self._top_message_container_locator)

    @property
    def is_top_message_container_displayed(self):
        return self.find_element(*self._top_message_container_locator).is_displayed()

    class Message(Region):

        _root_locator = (By.CLASS_NAME, "table-responsive")
        _title_locator = (By.CSS_SELECTOR, "a[href*='/messages/show/']")
        _checkbox_locator = (By.ID, "items_")

        @property
        def title(self):
            return self.find_element(*self._title_locator)

        @property
        def checkbox(self):
            return self.find_element(*self._checkbox_locator)
