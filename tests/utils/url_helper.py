from utils.bc_api_gateway import BCAPIGatewayBibs


class UrlHelper():
    def __init__(self, base_url):
        pass


class SearchPageHelper(UrlHelper):

    def search_page_specific(
            self, term, search_type, format,
            available, holds, base_url,
    ):

        """Will throw error if there are no items meeting the requirements"""
        item = BCAPIGatewayBibs(base_url).search(term.replace(" ", "+"), search_type=search_type, format=format,
                                                 available=available, holds=holds)

        search_page_specific_url = "{}/v2/search?custom_edit=true&query=li%3A{}&searchType=bl&suppress=true".format(
            base_url, item[0].id[:-3])

        """Returns a tuple of an advanced search URL using the items BidId
        as well as the item object(title, subtitle, id, type, availability,
        format, provider, eresourceURL) as returned from the Gateway"""
        return search_page_specific_url, item[0]
