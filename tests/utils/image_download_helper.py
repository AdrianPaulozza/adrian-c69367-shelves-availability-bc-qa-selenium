import os
import urllib.request

class ImageDownloadHelper():

    def download_image(self, url):
        image_full_name = "image.jpg"
        urllib.request.urlretrieve(url, image_full_name)

    def delete_downloaded_image(self, imageName):
        os.remove(os.getcwd() + "/" + imageName)
